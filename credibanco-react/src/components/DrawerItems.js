import React from 'react'
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import BookIcon from "@material-ui/icons/Book";
import ListItemText from "@material-ui/core/ListItemText";
import FaceIcon from "@material-ui/icons/Face";
import HomeWorkIcon from "@material-ui/icons/HomeWork";

const DrawerItems = ({setTitle, setStage}) => {
  return (
    <div>

      <Divider/>
      <List>
        <ListItem button onClick={() => { setTitle('Libros'); setStage(1)}}>
          <ListItemIcon>
            <BookIcon/>
          </ListItemIcon>
          <ListItemText primary={'Libros'}/>
        </ListItem>
        <ListItem button onClick={() => { setTitle('Autores'); setStage(2)}}>
          <ListItemIcon>
            <FaceIcon/>
          </ListItemIcon>
          <ListItemText primary={'Autores'}/>
        </ListItem>
        <ListItem button onClick={() => { setTitle('Editoriales'); setStage(3)}}>
          <ListItemIcon>
            <HomeWorkIcon />
          </ListItemIcon>
          <ListItemText primary={'Editoriales'}/>
        </ListItem>
      </List>

    </div>
  )
}

export default DrawerItems
