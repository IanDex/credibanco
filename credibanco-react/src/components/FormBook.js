import React from 'react';
import TextField from '@material-ui/core/TextField';
import {makeStyles} from '@material-ui/core/styles';
import {Button, Grid} from "@material-ui/core";
import Swal from 'sweetalert2'
import '../App.css';

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

const FormBook = () => {

  const [titulo, setTitulo] = React.useState('');
  const [anio, setAnio] = React.useState(0);
  const [genero, setGenero] = React.useState('');
  const [numPages, setNumPages] = React.useState(0);
  const [authorId, setAuthorId] = React.useState(0);
  const [editorialId, setEditorialId] = React.useState(0);

  const classes = useStyles();

  const handleSubmit = (e) => {
    Swal.showLoading();
    e.preventDefault();
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    let raw = JSON.stringify({
      "titulo": titulo,
      "anio": anio,
      "genero": genero,
      "numPages": numPages,
      "authorId": authorId,
      "editorialId": editorialId
    });
    console.log(raw)
    let requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("http://localhost:7000/api/books/create", requestOptions)
      .then(response => response.json())
      .then((result) => {
        const { code, message } = result;
        if(code === 1){
          Swal.fire({
            icon: 'success',
            title: 'Good',
            text: message,
          })
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: message,
          })
        }
      })
      .catch((error) => {
        console.log('error', error)
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: error,
        })
      });
  }

  return (
    <form className={classes.root} noValidate autoComplete="off" onSubmit={handleSubmit}>
      <div>
        <div className={classes.root}>
          <Grid container spacing={1}>
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={4}>
                <TextField
                  className={'w-100'}
                  onChange={(e) => {
                    setTitulo(e.target.value)
                  }}
                  name="titulo"
                  label="Titulo"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  className={'w-100'}
                  name="anio"
                  type={'number'}
                  onChange={(e) => {
                    setAnio(e.target.value)
                  }}
                  label="Año"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  onChange={(e) => {
                    setGenero(e.target.value)
                  }}
                  className={'w-100'}
                  name="genero"
                  label="Genero"
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={4}>
                <TextField
                  onChange={(e) => {
                    setNumPages(e.target.value)
                  }}
                  className={'w-100'}
                  name="numPages"
                  label="Número de Paginas"
                  type={'number'}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  onChange={(e) => {
                    setAuthorId(e.target.value)
                  }}
                  className={'w-100'}
                  name="autor"
                  label="Autor"
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  onChange={(e) => {
                    setEditorialId(e.target.value)
                  }}
                  className={'w-100'}
                  name="editorial"
                  label="Editorial"
                  variant="outlined"
                />
              </Grid>
            </Grid>
            <Grid container item>
              <Grid item xs={12}>
                <Button type={'submit'} variant="contained" color="primary">
                  Guardar
                </Button>
              </Grid>

            </Grid>

          </Grid>
        </div>
      </div>

    </form>
  )
}

export default FormBook
