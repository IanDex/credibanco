import React from 'react';
import TextField from '@material-ui/core/TextField';
import {makeStyles} from '@material-ui/core/styles';
import {Button, Grid} from "@material-ui/core";
import '../App.css';
import Swal from "sweetalert2";

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

const FormEditoriales = () => {

  const [nombre, setNombre] = React.useState('');
  const [direccionCpda, setdireccionCpda] = React.useState('');
  const [telefono, setTelefono] = React.useState(0);
  const [email, setEmail] = React.useState('');
  const [maxBooks, setMaxBooks] = React.useState(0);

  const handleSubmit = (e) => {
    Swal.showLoading();
    e.preventDefault();
    let myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    let raw = JSON.stringify({
      "nombre": nombre,
      "direccionCpda": direccionCpda,
      "telefono": telefono,
      "email": email,
      "maxBooks": maxBooks,
    });
    console.log(raw)
    let requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("http://localhost:7000/api/authors/create", requestOptions)
      .then(response => response.json())
      .then((result) => {
        const { code, message } = result;
        if(code === 1){
          Swal.fire({
            icon: 'success',
            title: 'Good',
            text: message,
          })
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: message,
          })
        }
      })
      .catch((error) => {
        console.log('error', error)
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: error,
        })
      });
  }

  const classes = useStyles();

  return (
    <form className={classes.root} noValidate autoComplete="off"  onSubmit={handleSubmit}>
      <div>
        <div className={classes.root}>
          <Grid container spacing={1}>
            <Grid container item xs={12} spacing={2}>
              <Grid item xs={6}>
                <TextField
                  className={'w-100'}
                  id="name"
                  onChange={(e) => {
                    setNombre(e.target.value)
                  }}
                  label="Nombre"
                  required
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  className={'w-100'}
                  id="direccionCpda"
                  type={'text'}
                  label="Dirección de Correspondencia"
                  variant="outlined"
                  onChange={(e) => {
                    setdireccionCpda(e.target.value)
                  }}
                />
              </Grid>

            </Grid>
            <Grid container item xs={12} spacing={2}>
              <Grid item xs={4}>

                <TextField
                  className={'w-100'}
                  id="telefono"
                  label="Telefono"
                  onChange={(e) => {
                    setTelefono(e.target.value)
                  }}
                  required
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  className={'w-100'}
                  id="email"
                  onChange={(e) => {
                    setEmail(e.target.value)
                  }}
                  label="Correo Electronico"
                  type={'email'}
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  className={'w-100'}
                  id="maxBooks"
                  label="Número Maximo de Libros"
                  type={'number'}
                  variant="outlined"
                  onChange={(e) => {
                    setMaxBooks(e.target.value)
                  }}
                />
              </Grid>

            </Grid>
            <Grid container item >
              <Grid item xs={12}>
                <Button type={'submit'} variant="contained" color="primary">
                  Guardar
                </Button>
              </Grid>

            </Grid>

          </Grid>
        </div>
      </div>
    </form>
  )
}

export default FormEditoriales
