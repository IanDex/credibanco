import React from 'react'
import Typography from "@material-ui/core/Typography";
import FormBook from "./FormBook";

const Stage1 = () => {
  return (
    <div>
      <Typography variant="h6" component="h2" gutterBottom>
        Registrar Libro
        <hr/>
      </Typography>
      <FormBook />
    </div>
  )
}

export default Stage1
