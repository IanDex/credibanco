import React from 'react'
import FormAutores from "./FormAutores";
import Typography from "@material-ui/core/Typography";

const Stage2 = () => {
  return (
    <div>
      <Typography variant="h6" component="h2" gutterBottom>
        Registrar Autores
        <hr/>
      </Typography>
      <FormAutores />
    </div>
  )
}

export default Stage2
