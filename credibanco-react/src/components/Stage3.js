import React from 'react'
import FormEditoriales from "./FormEditoriales";
import Typography from "@material-ui/core/Typography";

const Stage3 = () => {
  return (
    <div>
      <Typography variant="h6" component="h2" gutterBottom>
        Registrar Editoriales
        <hr/>
      </Typography>
      <FormEditoriales />
    </div>
  )
}

export default Stage3
