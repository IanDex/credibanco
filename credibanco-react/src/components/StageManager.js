import React from 'react'
import Stage1 from "./Stage1";
import Stage2 from "./Stage2";
import Stage3 from "./Stage3";

const StageManager = ({stage}) => {

  let opc;

  switch (stage) {
    case 1:
      opc = <Stage1/>
      break;
    case 2:
      opc = <Stage2/>
      break;
    case 3:
      opc = <Stage3/>
      break;

    default:
      break;
  }

  return opc;
}

export default StageManager
