package com.credibanco.assessment.library.api.client.exceptions;


import com.credibanco.assessment.library.commons.IGenericMessage;

public class ApplicationsExceptions extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Código de respuesta que se envía a la presentación del sistema
     */
    protected int code;

    /**
     * Descripción del error
     */
    protected String message;

    /**
     * Información adicional del error
     */
    protected Object data;
    /**
     * Constructor de la clase
     *
     * @param message
     *            Constante del error que está orriendo
     */
    public ApplicationsExceptions(IGenericMessage message) {
        this.code = message.getCode();
        this.message = message.getMessage();
    }

    public ApplicationsExceptions(IGenericMessage message, Object data) {
        this.code = message.getCode();
        this.message = message.getMessage();
        this.data = data;
    }

    public ApplicationsExceptions(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ApplicationsExceptions(IGenericMessage eMessage, String complement) {
        this(eMessage);
        message = eMessage.getMessage().replaceAll("__COMPLEMENTO__", complement);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
