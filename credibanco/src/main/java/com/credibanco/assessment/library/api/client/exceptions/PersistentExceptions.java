package com.credibanco.assessment.library.api.client.exceptions;

import com.credibanco.assessment.library.commons.IGenericMessage;

public class PersistentExceptions extends ApplicationsExceptions {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public PersistentExceptions(IGenericMessage message) {
        super(message);
    }

    public PersistentExceptions(IGenericMessage message, Object data) {
        super(message, data);
    }

    public PersistentExceptions(IGenericMessage eMessage, String complement) {
        super(eMessage);
        message = eMessage.getMessage().replaceAll("__COMPLEMENT__", complement);
    }

}
