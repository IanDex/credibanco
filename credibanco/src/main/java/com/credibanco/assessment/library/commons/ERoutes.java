package com.credibanco.assessment.library.commons;

public class ERoutes {

    public final static class App {
        public static final String AUTHORS = "/api/authors";
        public static final String BOOKS = "/api/books";
        public static final String EDITORIALS = "/api/editorials";
        public static final String CREATE = "/create";
        public static final String DELETE = "/delete/{id}";
        public static final String GET_ALL = "/get/all";
        public static final String GET_BY_ID = "/get/{id}";
        public static final String UPDATE = "/update";
    }

}
