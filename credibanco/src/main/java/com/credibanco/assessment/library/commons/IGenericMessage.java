package com.credibanco.assessment.library.commons;

public interface IGenericMessage {

    int getCode();
    String getMessage();
}

