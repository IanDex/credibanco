package com.credibanco.assessment.library.commons;

public enum IMessageStandard implements IGenericMessage {

    //Custom Messages
    OK(1, "Request successfully executed"),
    NO_RESULTS(0, "No results found"),

    //Errors General
    ERROR(-1, "Error processing request"),
    MAX_BOOKS(-2, "No es posible registrar el libro, se alcanzó el máximo permitido."),
    ERROR_AUTOR(-3, "El autor no está registrado"),
    ERROR_EDITORIAL(-3, "La editorial no está registrada"),

    //Server Errors
    ERROR_FATAL(-505, "Unexpected error, contact your system administrator"),

    //SQL ERRORS
    ERROR_INSERT(110, "Insert failed __COMPLEMENT__"),
    ERROR_UPDATE(111, "Update failed __COMPLEMENT__"),
    ERROR_DELETE(112, "Delete failed __COMPLEMENT__"),
    ERROR_NOT_FOUND(115, "No change was applied");

    private final int code;
    private final String message;

    private IMessageStandard(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
