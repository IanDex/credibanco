package com.credibanco.assessment.library.controller;

import com.credibanco.assessment.library.api.client.exceptions.PersistentExceptions;
import com.credibanco.assessment.library.commons.ERoutes;
import com.credibanco.assessment.library.exceptions.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.credibanco.assessment.library.service.AuthorsService;
import com.credibanco.assessment.library.dto.AuthorsDTO;

@RestController
@RequestMapping(value = ERoutes.App.AUTHORS)
@CrossOrigin("*")
public class AuthorsController {

    @Autowired
    AuthorsService authorsService;

    @PostMapping(ERoutes.App.CREATE)
    public ResponseDTO save(@RequestBody AuthorsDTO authorsDTO) throws PersistentExceptions {
        authorsService.save(authorsDTO);
        return new ResponseDTO();
    }

    @DeleteMapping(ERoutes.App.DELETE)
    public ResponseDTO delete(@PathVariable Integer id) throws PersistentExceptions {
        authorsService.delete(id);
        return new ResponseDTO();
    }

    @GetMapping(ERoutes.App.GET_ALL)
    public ResponseDTO getAllBooks() {
        return new ResponseDTO<>().setData(authorsService.getAll());
    }

    @GetMapping(ERoutes.App.GET_BY_ID)
    public ResponseDTO getByID(@PathVariable Integer id) throws PersistentExceptions {
        return new ResponseDTO<>().setData(authorsService.get(id));
    }

    @PutMapping(ERoutes.App.UPDATE)
    public ResponseDTO update(@RequestBody AuthorsDTO authorsDTO) throws PersistentExceptions {
        authorsService.update(authorsDTO);
        return new ResponseDTO();
    }

}
