package com.credibanco.assessment.library.controller;

import com.credibanco.assessment.library.api.client.exceptions.PersistentExceptions;
import com.credibanco.assessment.library.commons.ERoutes;
import com.credibanco.assessment.library.commons.IMessageStandard;
import com.credibanco.assessment.library.dto.BooksDTO;
import com.credibanco.assessment.library.exceptions.GenericController;
import com.credibanco.assessment.library.exceptions.ResponseDTO;
import com.credibanco.assessment.library.service.BooksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = ERoutes.App.BOOKS)
@CrossOrigin("*")
public class BooksController extends GenericController {

    @Autowired
    BooksService booksService;

    @PostMapping(ERoutes.App.CREATE)
    public ResponseDTO saveBook(@RequestBody BooksDTO book) throws PersistentExceptions {
        booksService.save(book);
        return new ResponseDTO();
    }

    @DeleteMapping(ERoutes.App.DELETE)
    public ResponseDTO delete(@PathVariable Integer id) throws PersistentExceptions {
        booksService.delete(id);
        return new ResponseDTO();
    }

    @GetMapping(ERoutes.App.GET_ALL)
    public ResponseDTO getAllBooks() {
        return new ResponseDTO<>().setData(booksService.getAll());
    }

    @GetMapping(ERoutes.App.GET_BY_ID)
    public ResponseDTO getByID(@PathVariable Integer id) throws PersistentExceptions {
        return new ResponseDTO<>().setData(booksService.get(id));
    }

    @PutMapping(ERoutes.App.UPDATE)
    public ResponseDTO update(@RequestBody BooksDTO booksDTO) throws PersistentExceptions {
        booksService.update(booksDTO);
        return new ResponseDTO();
    }

}
