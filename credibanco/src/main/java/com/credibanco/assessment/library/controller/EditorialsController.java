package com.credibanco.assessment.library.controller;

import com.credibanco.assessment.library.api.client.exceptions.PersistentExceptions;
import com.credibanco.assessment.library.commons.ERoutes;
import com.credibanco.assessment.library.exceptions.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.credibanco.assessment.library.service.EditorialsService;
import com.credibanco.assessment.library.dto.EditorialsDTO;

@RestController
@RequestMapping(value = ERoutes.App.EDITORIALS)
@CrossOrigin("*")
public class EditorialsController {

    @Autowired
    EditorialsService editorialsService;

    @PostMapping(ERoutes.App.CREATE)
    public ResponseDTO save(@RequestBody EditorialsDTO editorialsDTO) throws PersistentExceptions {
        editorialsService.save(editorialsDTO);
        return new ResponseDTO();
    }

    @DeleteMapping(ERoutes.App.DELETE)
    public ResponseDTO delete(@PathVariable Integer id) throws PersistentExceptions {
        editorialsService.delete(id);
        return new ResponseDTO();
    }

    @GetMapping(ERoutes.App.GET_ALL)
    public ResponseDTO getAllBooks() {
        return new ResponseDTO<>().setData(editorialsService.getAll());
    }

    @GetMapping(ERoutes.App.GET_BY_ID)
    public ResponseDTO getByID(@PathVariable Integer id) throws PersistentExceptions {
        return new ResponseDTO<>().setData(editorialsService.get(id));
    }

    @PutMapping(ERoutes.App.UPDATE)
    public ResponseDTO update(@RequestBody EditorialsDTO editorialsDTO) throws PersistentExceptions {
        editorialsService.update(editorialsDTO);
        return new ResponseDTO();
    }

}
