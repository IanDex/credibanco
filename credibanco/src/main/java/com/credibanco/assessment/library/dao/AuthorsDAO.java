package com.credibanco.assessment.library.dao;

import com.credibanco.assessment.library.model.Authors;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorsDAO extends JpaRepository<Authors, Integer>{

    boolean existsById(Integer id);

}