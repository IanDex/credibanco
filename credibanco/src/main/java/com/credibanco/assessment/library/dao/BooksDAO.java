package com.credibanco.assessment.library.dao;

import com.credibanco.assessment.library.model.Books;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BooksDAO extends JpaRepository<Books, Integer> {

    Integer countByEditorialId(Integer id);

}
