package com.credibanco.assessment.library.dao;

import com.credibanco.assessment.library.model.Editorials;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EditorialsDAO extends JpaRepository<Editorials, Integer> {

    boolean existsById(Integer id);

}