package com.credibanco.assessment.library.dto;

public class EditorialsDTO {

    private Integer id;
    private String nombre;
    private String direccionCpda;
    private Long telefono;
    private String email;
    private Integer maxBooks;

    public EditorialsDTO() {
        super();
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccionCpda() {
        return this.direccionCpda;
    }

    public void setDireccionCpda(String direccionCpda) {
        this.direccionCpda = direccionCpda;
    }

    public Long getTelefono() {
        return this.telefono;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getMaxBooks() {
        return this.maxBooks;
    }

    public void setMaxBooks(Integer maxBooks) {
        this.maxBooks = maxBooks;
    }

}