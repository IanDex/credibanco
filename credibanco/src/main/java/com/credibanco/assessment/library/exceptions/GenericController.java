package com.credibanco.assessment.library.exceptions;

import com.credibanco.assessment.library.api.client.exceptions.ApplicationsExceptions;
import com.credibanco.assessment.library.api.client.exceptions.PersistentExceptions;
import com.credibanco.assessment.library.commons.IMessageStandard;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

public class GenericController {

    @ExceptionHandler({
            ApplicationsExceptions.class,
            PersistentExceptions.class,
    })
    @ResponseStatus(HttpStatus.OK)
    public ResponseDTO controlError(ApplicationsExceptions e) {
        return new ResponseDTO(e.getCode(), e.getMessage()).setData(e.getData());
    }

    @ExceptionHandler({
            NumberFormatException.class
    })
    @ResponseStatus(HttpStatus.OK)
    public ResponseDTO controlError(NumberFormatException e) {
        return new ResponseDTO(-1, "Error to convert");
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseDTO handleException(MethodArgumentNotValidException ex) {
        List<ObjectError> listErrors = ex.getBindingResult().getAllErrors();
        StringBuilder messages = new StringBuilder();

        for (ObjectError error : listErrors) {
            messages.append(error.getDefaultMessage())
                    .append(" ");
        }
        return new ResponseDTO(IMessageStandard.ERROR).setMessage(messages.toString());
    }

    @ExceptionHandler({
            JsonProcessingException.class,
            MismatchedInputException.class,
            JsonMappingException.class
    })
    @ResponseStatus(HttpStatus.OK)
    public ResponseDTO controlError(JsonProcessingException ex) {
        return new ResponseDTO(IMessageStandard.ERROR);
    }

    @ExceptionHandler({
            Exception.class
    })
    @ResponseStatus(HttpStatus.OK)
    public ResponseDTO controlError(Throwable ex) {
        return new ResponseDTO(IMessageStandard.ERROR_FATAL);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseDTO controlError() {
        System.out.println("500");
        return new ResponseDTO(IMessageStandard.ERROR_FATAL);
    }


}
