package com.credibanco.assessment.library.exceptions;

import com.credibanco.assessment.library.commons.IGenericMessage;
import com.credibanco.assessment.library.commons.IMessageStandard;

import java.util.List;

public class ResponseDTO<T> {

    private int code;
    private String message;
    private T data;

    public ResponseDTO() {
        this(IMessageStandard.OK);
    }

    public ResponseDTO(IGenericMessage iMessage) {
        setResponse(iMessage);
    }

    private void setResponse(IGenericMessage iMessage) {
        this.code = iMessage.getCode();
        this.message = iMessage.getMessage();
    }

    public ResponseDTO(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public ResponseDTO<T> setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ResponseDTO<T> setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public ResponseDTO<T> setData(T data) {
        this.data = data;
        if (data instanceof List) {
            validList((List) data);
        }
        return this;
    }

    private void validList(List data) {
        if (data == null || data.isEmpty()) {
            setResponse(IMessageStandard.NO_RESULTS);
            this.data = null;
        }
    }

}
