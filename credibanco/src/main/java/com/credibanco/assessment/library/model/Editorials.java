package com.credibanco.assessment.library.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "editorials")
public class Editorials implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "some_seq_gen_name")
    @SequenceGenerator(name = "some_seq_gen_name", sequenceName = "SOME_SEQ", allocationSize = 1)
    private Integer id;

    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "direccion_cpda")
    private String direccionCpda;

    @Column(name = "telefono", nullable = false)
    private Long telefono;

    private String email;

    @Column(columnDefinition = "integer default -1")
    private Integer maxBooks;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccionCpda() {
        return direccionCpda;
    }

    public void setDireccionCpda(String direccionCpda) {
        this.direccionCpda = direccionCpda;
    }

    public Long getTelefono() {
        return telefono;
    }

    public void setTelefono(Long telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getMaxBooks() {
        return maxBooks;
    }

    public void setMaxBooks(Integer maxBooks) {
        this.maxBooks = maxBooks;
    }

}
