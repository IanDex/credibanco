package com.credibanco.assessment.library.service;

import com.credibanco.assessment.library.api.client.exceptions.PersistentExceptions;
import com.credibanco.assessment.library.dto.AuthorsDTO;

import java.util.List;

public interface AuthorsService {

    void save(AuthorsDTO authorsDTO) throws PersistentExceptions;

    void update(AuthorsDTO authorsDTO) throws PersistentExceptions;

    void delete(Integer id) throws PersistentExceptions;

    AuthorsDTO get(Integer id) throws PersistentExceptions;

    List<AuthorsDTO> getAll();

}

