package com.credibanco.assessment.library.service;

import com.credibanco.assessment.library.api.client.exceptions.PersistentExceptions;
import com.credibanco.assessment.library.dto.BooksDTO;

import java.util.List;

public interface BooksService {

    void save(BooksDTO booksDTO) throws PersistentExceptions;

    void update(BooksDTO booksDTO) throws PersistentExceptions;

    void delete(Integer id) throws PersistentExceptions;

    BooksDTO get(Integer id) throws PersistentExceptions;

    List<BooksDTO> getAll();

}
