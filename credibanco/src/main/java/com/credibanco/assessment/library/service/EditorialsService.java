package com.credibanco.assessment.library.service;

import com.credibanco.assessment.library.api.client.exceptions.PersistentExceptions;
import com.credibanco.assessment.library.dto.EditorialsDTO;

import java.util.List;

public interface EditorialsService {

    void save(EditorialsDTO editorialsDTO) throws PersistentExceptions;

    void update(EditorialsDTO editorialsDTO) throws PersistentExceptions;

    void delete(Integer id) throws PersistentExceptions;

    EditorialsDTO get(Integer id) throws PersistentExceptions;

    List<EditorialsDTO> getAll();

}

