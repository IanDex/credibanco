package com.credibanco.assessment.library.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

import com.credibanco.assessment.library.api.client.exceptions.PersistentExceptions;
import com.credibanco.assessment.library.commons.IMessageStandard;
import com.credibanco.assessment.library.model.Books;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.credibanco.assessment.library.dao.AuthorsDAO;
import com.credibanco.assessment.library.model.Authors;
import com.credibanco.assessment.library.service.AuthorsService;
import com.credibanco.assessment.library.dto.AuthorsDTO;

@Service
public class AuthorsServiceImpl implements AuthorsService {

    @Autowired
    AuthorsDAO authorsDAO;

    @Override
    @Transactional
    public void save(AuthorsDTO authorsDTO) throws PersistentExceptions {
        try {
            Authors authors = new Authors();
            BeanUtils.copyProperties(authorsDTO, authors);
            authorsDAO.save(authors);
        } catch (Exception ex) {
            throw new PersistentExceptions(IMessageStandard.ERROR_INSERT, ex.getMessage());
        }
    }

    @Override
    @Transactional
    public void update(AuthorsDTO authorsDTO) throws PersistentExceptions {
        Integer id = authorsDTO.getId();
        Boolean objectExists = authorsDAO.existsById(id);
        if (objectExists) {
            try {
                Authors authors = new Authors();
                BeanUtils.copyProperties(authorsDTO, authors);
                authorsDAO.save(authors);
            } catch (Exception ex) {
                throw new PersistentExceptions(IMessageStandard.ERROR_UPDATE, ex.getMessage());
            }
        } else {
            throw new PersistentExceptions(IMessageStandard.ERROR_NOT_FOUND);
        }
    }

    @Override
    @Transactional
    public void delete(Integer id) throws PersistentExceptions {
        Boolean objectExists = authorsDAO.existsById(id);
        if (objectExists) {
            try {
                authorsDAO.deleteById(id);
            } catch (Exception ex) {
                throw new PersistentExceptions(IMessageStandard.ERROR_DELETE, ex.getMessage());
            }
        } else {
            throw new PersistentExceptions(IMessageStandard.ERROR_NOT_FOUND);
        }
    }

    @Override
    @Transactional
    public AuthorsDTO get(Integer id) throws PersistentExceptions {
        Optional<Authors> authorsOptional = authorsDAO.findById(id);
        AuthorsDTO authorsDTO = null;
        if (authorsOptional.isPresent()) {
            authorsDTO = new AuthorsDTO();
            BeanUtils.copyProperties(authorsOptional.get(), authorsDTO);
        } else {
            throw new PersistentExceptions(IMessageStandard.ERROR_NOT_FOUND);
        }

        return authorsDTO;
    }

    @Override
    @Transactional
    public List<AuthorsDTO> getAll() {
        List<Authors> authorsList = authorsDAO.findAll();
        List<AuthorsDTO> authorsDTOList = new ArrayList<>();
        if (!authorsList.isEmpty()) {
            for (Authors authors : authorsList) {
                AuthorsDTO authorsDTO = new AuthorsDTO();
                BeanUtils.copyProperties(authors, authorsDTO);
                authorsDTOList.add(authorsDTO);
            }
        }
        return authorsDTOList;
    }

}

