package com.credibanco.assessment.library.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.credibanco.assessment.library.api.client.exceptions.PersistentExceptions;
import com.credibanco.assessment.library.commons.IMessageStandard;
import com.credibanco.assessment.library.dao.AuthorsDAO;
import com.credibanco.assessment.library.dao.BooksDAO;
import com.credibanco.assessment.library.dao.EditorialsDAO;
import com.credibanco.assessment.library.dto.AuthorsDTO;
import com.credibanco.assessment.library.dto.EditorialsDTO;
import com.credibanco.assessment.library.model.Authors;
import com.credibanco.assessment.library.model.Editorials;
import com.credibanco.assessment.library.service.EditorialsService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.credibanco.assessment.library.service.BooksService;
import com.credibanco.assessment.library.dto.BooksDTO;
import com.credibanco.assessment.library.model.Books;

@Service
public class BooksServiceImpl implements BooksService {

    private BooksDAO booksDao;
    private AuthorsDAO authorsDAO;
    private EditorialsDAO editorialsDAO;

    @Autowired
    private EditorialsService editorialsService;


    @Autowired
    public BooksServiceImpl(BooksDAO booksDao, AuthorsDAO authorsDAO, EditorialsDAO editorialsDAO) {
        this.booksDao = booksDao;
        this.authorsDAO = authorsDAO;
        this.editorialsDAO = editorialsDAO;
    }

    @Override
    @Transactional
    public void save(BooksDTO booksDTO) throws PersistentExceptions {
        try {
            if (!authorsDAO.existsById(booksDTO.getAuthorId()))
                throw new PersistentExceptions(IMessageStandard.ERROR_AUTOR);
            if (!editorialsDAO.existsById(booksDTO.getEditorialId()))
                throw new PersistentExceptions(IMessageStandard.ERROR_EDITORIAL);

            EditorialsDTO editorialsDTO = editorialsService.get(booksDTO.getEditorialId());
            if (booksDao.countByEditorialId(booksDTO.getEditorialId()) < editorialsDTO.getMaxBooks()) {
                Books books = new Books();
                BeanUtils.copyProperties(booksDTO, books);
                booksDao.save(books);
            } else {
                throw new PersistentExceptions(IMessageStandard.MAX_BOOKS);
            }
        } catch (Exception ex) {
            throw new PersistentExceptions(IMessageStandard.ERROR_INSERT, ex.getMessage());
        }
    }

    @Override
    @Transactional
    public void update(BooksDTO booksDTO) throws PersistentExceptions {
        Integer id = booksDTO.getId();
        boolean objectExists = booksDao.existsById(id);
        if (objectExists) {
            try {
                Books books = new Books();
                BeanUtils.copyProperties(booksDTO, books);
                booksDao.save(books);
            } catch (Exception ex) {
                throw new PersistentExceptions(IMessageStandard.ERROR_UPDATE, ex.getMessage());
            }
        } else {
            throw new PersistentExceptions(IMessageStandard.ERROR_NOT_FOUND);
        }
    }

    @Override
    @Transactional
    public void delete(Integer id) throws PersistentExceptions {
        boolean objectExists = booksDao.existsById(id);
        if (objectExists) {
            try {
                booksDao.deleteById(id);
            } catch (Exception ex) {
                throw new PersistentExceptions(IMessageStandard.ERROR_DELETE, ex.getMessage());
            }
        } else {
            throw new PersistentExceptions(IMessageStandard.ERROR_NOT_FOUND);
        }
    }

    @Override
    @Transactional
    public BooksDTO get(Integer id) throws PersistentExceptions {
        Optional<Books> booksOptional = booksDao.findById(id);
        BooksDTO booksDTO = null;
        if (booksOptional.isPresent()) {
            booksDTO = new BooksDTO();
            BeanUtils.copyProperties(booksOptional.get(), booksDTO);
        } else {
            throw new PersistentExceptions(IMessageStandard.ERROR_NOT_FOUND);
        }

        return booksDTO;
    }

    @Override
    @Transactional
    public List<BooksDTO> getAll() {
        List<Books> booksList = booksDao.findAll();
        List<BooksDTO> booksDTOList = new ArrayList<>();
        if (!booksList.isEmpty()) {
            for (Books books : booksList) {
                BooksDTO booksDTO = new BooksDTO();
                BeanUtils.copyProperties(books, booksDTO);
                booksDTOList.add(booksDTO);
            }
        }
        return booksDTOList;
    }

}

