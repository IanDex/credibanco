package com.credibanco.assessment.library.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.credibanco.assessment.library.api.client.exceptions.PersistentExceptions;
import com.credibanco.assessment.library.commons.IMessageStandard;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.credibanco.assessment.library.dao.EditorialsDAO;
import com.credibanco.assessment.library.model.Editorials;
import com.credibanco.assessment.library.service.EditorialsService;
import com.credibanco.assessment.library.dto.EditorialsDTO;

@Service
public class EditorialsServiceImpl implements EditorialsService {

    @Autowired
    EditorialsDAO editorialsDAO;

    @Override
    @Transactional
    public void save(EditorialsDTO editorialsDTO) throws PersistentExceptions {
        try {
            Editorials editorials = new Editorials();
            BeanUtils.copyProperties(editorialsDTO, editorials);
            editorialsDAO.save(editorials);
        } catch (Exception ex) {
            throw new PersistentExceptions(IMessageStandard.ERROR_INSERT, ex.getMessage());
        }
    }

    @Override
    @Transactional
    public void update(EditorialsDTO editorialsDTO) throws PersistentExceptions {
        Integer id = editorialsDTO.getId();
        boolean objectExists = editorialsDAO.existsById(id);
        if (objectExists) {
            try {
                Editorials editorials = new Editorials();
                BeanUtils.copyProperties(editorialsDTO, editorials);
                editorialsDAO.save(editorials);
            } catch (Exception ex) {
                throw new PersistentExceptions(IMessageStandard.ERROR_UPDATE, ex.getMessage());
            }
        } else {
            throw new PersistentExceptions(IMessageStandard.ERROR_NOT_FOUND);
        }
    }

    @Override
    @Transactional
    public void delete(Integer id) throws PersistentExceptions {
        if (editorialsDAO.existsById(id)) {
            try {
                editorialsDAO.deleteById(id);
            } catch (Exception ex) {
                throw new PersistentExceptions(IMessageStandard.ERROR_DELETE, ex.getMessage());
            }
        } else {
            throw new PersistentExceptions(IMessageStandard.ERROR_NOT_FOUND);
        }
    }

    @Override
    @Transactional
    public EditorialsDTO get(Integer id) throws PersistentExceptions {
        Optional<Editorials> editorialsOptional = editorialsDAO.findById(id);
        EditorialsDTO editorialsDTO = null;
        if (editorialsOptional.isPresent()) {
            editorialsDTO = new EditorialsDTO();
            BeanUtils.copyProperties(editorialsOptional.get(), editorialsDTO);
        } else {
            throw new PersistentExceptions(IMessageStandard.ERROR_NOT_FOUND);
        }

        return editorialsDTO;
    }

    @Override
    @Transactional
    public List<EditorialsDTO> getAll() {
        List<Editorials> editorialsList = editorialsDAO.findAll();
        List<EditorialsDTO> editorialsDTOList = new ArrayList<>();
        if (!editorialsList.isEmpty()) {
            for (Editorials editorials : editorialsList) {
                EditorialsDTO editorialsDTO = new EditorialsDTO();
                BeanUtils.copyProperties(editorials, editorialsDTO);
                editorialsDTOList.add(editorialsDTO);
            }
        }
        return editorialsDTOList;
    }

}

